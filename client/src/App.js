import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    
    this.state = {
      hospitals: []
    }
  }

  async componentDidMount() {
    const response = await fetch('/api/hospitals')
    const hospitals   = await response.json()

    this.setState({ hospitals })
  }

  render() {
    return (
      <div>
        <h2>Hastaneler</h2>
        <ul>
          {this.state.hospitals.map( hospital => {
            return (
	      <ul key={hospital._id}>
		<li>======================</li>
		<li><b>Hastanenin Adı: </b> {hospital.name}</li>
		<li><b>Hastanenin Adresi: </b> {hospital.address}</li>
		<li><b>Hastanenin websitesi: </b> <a href={hospital.website} target="_blank">{hospital.website}</a></li>
		<li>======================</li>
		<li></li>
	      </ul>
	    )
          })}
        </ul>
      </div>
    );
  }
}

export default App;
