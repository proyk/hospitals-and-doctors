## Hastaneler ve Doktorlar

- mongodb yi yükle
- mongodb yi çalıştır `sudo service mongod start`
- yarn install
- yarn start

servisler;

localhost:3001/api/hospitals/:limit // Hastaneleri verir.
  - Örnek localhost:3001/api/hospitals/20 // 20 tane hastane verir.

localhost:3001/api/doctors/:url // İstenilen hastanenin doktorlarını verir.
  - Örnek localhost:3001/api/doctors/www.buyukanadoluhastanesi.com // Büyük Anadolu Hastanesinin doktorlarını verir.