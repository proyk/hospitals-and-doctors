import mongoose from 'mongoose';

const Hospitals = mongoose.model('Hospitals');

export const newHospital = ({ name, address, placeId, website }) => {
  Hospitals.create({
    name,
    address,
    placeId,
    website
  }, (err) => {
    if (err) {
      console.log('Hospital create error: ', err);
      strOutput = 'Oh dear, we\'ve got an error';
    }
  });
};

export const getHospitals = ({ limit = 10 }) => {
  return new Promise((resolve, reject) => {
    Hospitals.find()
      .limit(limit)
      .exec((err, docs) => {
	if (err) {
	  strOutput = 'Oh dear, we\'ve got an error';
	  return reject('Hospital create error: ', err);
	}
	
	return resolve(docs);
      });
  });
};

export const updateHospital = (find, update, options) => {
  Hospitals.update({ ...find }, { ...update }, (err) => {
    if (err) {
      console.log('Hospital update status error: ', err);
      strOutput = 'Oh dear, we\'ve got an error';
    }
  })
};
