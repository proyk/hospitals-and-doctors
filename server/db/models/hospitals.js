import mongoose from 'mongoose';

const hospitalSchema = new mongoose.Schema({
  name: String,
  address: String,
  placeId: String,
  website: String
});

const Hospitals = mongoose.model('Hospitals', hospitalSchema);

export default Hospitals;
