import axios from 'axios';
import { newHospital, updateHospital } from '../methods/hospitals';

export const saveHospitalsToDB = (nextPageToken) => {  
  if (nextPageToken) {
    const timeout = 3000;

    setTimeout(() => {
      console.log('Waiting', timeout, 'ms');
      _requestWithRetry(`https://maps.googleapis.com/maps/api/place/textsearch/json?pagetoken=${nextPageToken}`);
    }, timeout);
    
  } else {
    _requestWithRetry('https://maps.googleapis.com/maps/api/place/textsearch/json?query=hastane&type=hospital&language=tr');
  }
};

const _requestWithRetry = (uri) => {
  const url = `${uri}&key=${process.env.GOOGLE_API_KEY}`;

  axios({ method:'get', url })
    .then((res) => {
      if (res.data && res.data.status && res.data.status === 'OK') {
	const hospitals = res.data.results.forEach(hospital => {
	  getHospitalsWebsite(hospital.place_id).then(placeDetails => {
	    let website;
	    
	    if (placeDetails.data && placeDetails.data.status && placeDetails.data.status === 'OK') {
	      website = !!placeDetails.data.result && placeDetails.data.result.website || '';
	    };

	    newHospital({ name: hospital.name, address: hospital.formatted_address, placeId: hospital.place_id, website });
	  }).catch(err => console.log(err));
	});

	if (res.data.next_page_token) {
	  saveHospitalsToDB(res.data.next_page_token);
	}
      };
    })
    .catch((err) => {
      console.log('Google place api error', err);
    });
};

const getHospitalsWebsite = (placeId) => {
  const url = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${placeId}&key=${process.env.GOOGLE_API_KEY}`;
  return axios.get(url);
};
