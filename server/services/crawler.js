import Crawler from 'simplecrawler';
import cheerio from 'cheerio';

export const getDoctors = ({ url }) => {
  return new Promise((resolve, reject) => {
    
    var crawler = new Crawler(`http://${url}`);

    const doctors = [];
    
    crawler.discoverResources = function(buffer, queueItem) {
      var $ = cheerio.load(buffer.toString("utf8"));

      return $("a[href]").map(function () {
        return /(doktor|hekim|doctor)/i.test($(this).attr("href")) && $(this).attr("href");
      }).get();
    };
    
    crawler.on("fetchcomplete", function(queueItem, responseBuffer, response) {
      console.log("I just received %s", queueItem.url);

      var $ = cheerio.load(responseBuffer.toString("utf8"));
      
      const doctor = $('h4').filter(function(i, el) {
        return /(dr.|op.dr.|dr|doç.dr.|uzm.dr.|uz.dr.|prof.dr.|prof|dr|doc|uzm|uz)/i.test($(this).text());
      }).first().text();

      if (doctor) {
	console.log('DOKTOR: %s', doctor);
	//doctors.push({ name: doctor });
      }
    });
    
    crawler.on("complete", function() {
      console.log("Finished!");
      resolve(doctors);
    });

    crawler.on("fetcherror", function(err) {
      reject(err);
    });
    
    crawler.start();
  });
};

