import { getHospitals } from './methods/hospitals';
import { saveHospitalsToDB } from './services/google_place';

getHospitals({ limit: 1 })
  .then(hospitals => {
    if (hospitals.length === 0) {
      saveHospitalsToDB();
    }
  })
  .catch(err => {
    console.log('error get hospitals', err);
  });
