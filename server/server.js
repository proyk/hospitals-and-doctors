import bodyParser from 'body-parser';
import express from 'express';
import path from 'path';


// connect mongodb
import './db';

// seeds
import './seeds';

import { getHospitals } from './methods/hospitals';
import { getDoctors } from './services/crawler';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const router = express.Router();

const staticFiles = express.static(path.join(__dirname, '../../client/build'));
app.use(staticFiles);

router.get('/api/hospitals/:limit', (request, response) => {
  const limit = parseInt(request.params.limit, 10) || 20;
  
  getHospitals({ limit })
    .then(hospitals => response.json(hospitals))
    .catch((err) => {
      response.send(err);
    });
});

router.get('/api/doctors/:url', (request, response) => {
  const url = request.params.url;

  getDoctors({ url })
    .then((doctors) => {
      response.json(doctors);
    })
    .catch((err) => {
      response.send(err);
    });
});

app.use(router);

// any routes not picked up by the server api will be handled by the react router
app.use('/*', staticFiles);

app.set('port', (process.env.PORT || 3001));
app.listen(app.get('port'), () => {
  console.log(`Listening on ${app.get('port')}`);
});
